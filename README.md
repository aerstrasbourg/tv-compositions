# Compositions
A microservice to manage compositions for the TV project. It allows you to create some compositions of modules, like a mosaic of pictures.

# API
### POST /composition
Create a new composition.

Request body:
```
{
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    }
}
```

Response body:
```
{
    "_id": "555cac8d5d01b76811000000",
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    },
    "subModules": [
        ...
    ]
}
```

### GET /composition
Get all compositions.

Request body:
```
{
}
```

Response body:
```
[
    {
        "_id": "555cac8d5d01b76811000000",
        "type": "test type",
        "name": "test name",
        "config": {
            ...
        },
        "subModules": [
            ...
        ]

    },
    ...
]
```

### GET /composition/:id

Get a composition.

Request body:
```
{
}
```

Response body:
```
{
    "_id": "555cac8d5d01b76811000000",
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    },
    "subModules": [
        ...
    ]
}
```

### DELETE /composition/:id

Delete a composition.

Request body
```
{
}
```

Response body
```
{
}
```

### POST /composition/:id/module/:mod/slot/:slot

Create a new module in the module :mod at the slot :slot.

Request body:
```
{
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    },
    "subModules": [
        ...
    ]
}
```

Response body:
```
{
    "_id": "555cac8d5d01b76811000000",
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    },
    "subModules": [
        ...
    ]
}
```

### PATCH /composition/:id/module/:mod

Update a module.

Request body:
```
{
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    },
    "subModules": [
        ...
    ]
}
```

Response body:
```
{
    "_id": "555cac8d5d01b76811000000",
    "type": "test type",
    "name": "test name",
    "config": {
        ...
    },
    "subModules": [
        ...
    ]
}
```

### DELETE /composition/:id/module/:mod/ ###

Delete the module :mod in the composition.

Request body:
```
{
}
```

Response body:
```
{
}
```

# Documentation
A [documentation](aerstrasbourg.bitbucket.org/tv-compositions/) is available for further informations.

# Author
Developed by Matthieu KERN, matthieukern@gmail.com
