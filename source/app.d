/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module app;

import std.stdio;
import std.exception;

import vibe.d;

import compositions.api;
import compositions.utils;

void addACAO(HTTPServerRequest req, HTTPServerResponse res) {
    res.headers["Access-Control-Allow-Origin"] = "*";
    res.headers["Access-Control-Allow-Credentials"] = "true";
    res.headers["Access-Control-Allow-Methods"] = "PROPFIND, PROPPATCH, COPY, MOVE, DELETE, MKCOL, LOCK, UNLOCK, PUT, GETLIB, VERSION-CONTROL, CHECKIN, CHECKOUT, UNCHECKOUT, REPORT, UPDATE, CANCELUPLOAD, HEAD, OPTIONS, GET, POST, PATCH";
    res.headers["Access-Control-Allow-Headers"] = "Overwrite, Destination, Content-Type, Depth, User-Agent, X-File-Size, X-Requested-With, If-Modified-Since, X-File-Name, Cache-Control";
}

void sendOptions(HTTPServerRequest req, HTTPServerResponse res) {
    addACAO(req, res);
    res.writeBody("");
}

void logRequests(HTTPServerRequest req, HTTPServerResponse res) {
    logInfo("[" ~ to!string(req.method) ~ "] " ~ req.path);
}

/++
    Entry point of the program.

    It defines the server configurations, initialize what must be initialized,
    set the server's configs, and launch the API.
 +/
shared static this() {
    setLogLevel(LogLevel.debug_);
    // HTTP server settings
    logInfo("Preparing the HTTP server's settings...");
    auto settings = new HTTPServerSettings;
    settings.port = cast(ushort) to!int(Params.get("SERV_PORT"));
    settings.bindAddresses = [Params.get("SERV_ADDRESS")];

    // SSL certificates
    logInfo("Creating the SSL context for the HTTPS connexion...");
    auto sslctx = createSSLContext(SSLContextKind.server);
    sslctx.useCertificateChainFile("certificates/server.crt");
    sslctx.usePrivateKeyFile("certificates/server.key");
    settings.sslContext = sslctx;

    // API router
    logInfo("Initializing the router for the API...");
    auto router = new URLRouter;
    router.any("*", &addACAO);
    router.any("*", &logRequests);
    router.get("*", serveStaticFiles("./public/"));
    router.match(HTTPMethod.OPTIONS, "*", &sendOptions);
    router.registerRestInterface(new APIBase);

    logDebug("");
    logDebug("Server settings:");
    logDebug("\tPort: ", to!int(Params.get("SERV_PORT")));
    logDebug("\tAddress: ", Params.get("SERV_ADDRESS"));

    logDebug("");
    logDebug("MongoDB settings:");
    logDebug("\tHost: " ~ Params.get("MG_HOST"));
    logDebug("\tPort: " ~ to!string(to!int(Params.get("MG_PORT"))));
    logDebug("\tUser: " ~ Params.get("MG_USER"));
    logDebug("\tPassword: " ~ Params.get("MG_PWD"));
    logDebug("\tDatabase: " ~ Params.get("MG_DB"));
    logDebug("\tOptions: " ~ Params.get("MG_OPT"));

    logDebug("");
    logDebug("Routes:");
    auto routes = router.getAllRoutes();
    foreach (r ; routes) {
        if (r.pattern != "*")
            logDebug("\t", r.method, " \t- ", r.pattern);
    }
    logDebug("");

    // Running HTTP listening.
    logInfo("Starting HTTP listening...");
    listenHTTP(settings, router);
    logInfo("");
}
