/++
    Authors: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: Subject to the terms of the MIT license, as written in the included LICENSE file.
 +/
module mongorm.datastructure.base;

import std.traits;
import std.random;
import std.conv;
import std.stdio;
import std.range;
import std.ascii;
import std.datetime;

public import vibe.d;

import mongorm.storage;

enum : string { update = "updatableField" };

///
template Store(T) {
    mixin("import " ~ moduleName!T ~ ";");

    /++
        Generate the update string for the updatable fields.
    +/
    private string update() {
        string ret = "";
        foreach(name; __traits(allMembers, T)) {
            static if (__traits(getProtection, mixin("T." ~ name)) == "public") {
                foreach(symbol; __traits(getOverloads, T, name)) {
                    static if (isCallable!symbol && (functionAttributes!symbol & FunctionAttribute.property)) {
                        foreach (UDA; __traits(getAttributes, symbol)) {
                            static if (UDA == "updatableField") {
                                ret ~= "if (__updatableFields[\"" ~ name ~ "\"] == false && (cast(" ~ T.stringof ~ ")this)." ~ name ~ " != res." ~ name ~ ")" ~
                                    "(cast(" ~ T.stringof ~ ")this)." ~ name ~ " = res." ~ name ~ ";";
                            }
                        }
                    }
                }
            }
        }
        return ret;
    }

    /++
        Save the entity to the datastore.

        If the entity doesn't exists in the datastore, then inserts it.

        If the entity do exists in the datastore, then update it's modified fields, and
        reload it from the datastore.
    +/
    private string store() {
        string ret =
            "void store() {" ~ q{
                MongoCollection collection = MGCollection(_collectionName);
                Bson req = Bson.emptyObject;} ~
                "if (" ~ q{this._id == BsonObjectID.fromString("000000000000000000000000")} ~ ") {" ~
                    q{this._id = BsonObjectID.generate;}
                    "req = serializeToBson(cast(" ~ T.stringof ~ ")this);" ~
                    q{
                    logDebug("Saving entity in collection \"" ~ _collectionName ~ "\" with value " ~ req.toString ~ ".");
                    collection.insert(req);
                    } ~
                "} else {";
            foreach(name; __traits(allMembers, T)) {
                static if (__traits(getProtection, mixin("T." ~ name)) == "public") {
                    foreach(symbol; __traits(getOverloads, T, name)) {
                        static if (isCallable!symbol && (functionAttributes!symbol & FunctionAttribute.property)) {
                            foreach (uda; __traits(getAttributes, symbol)) {
                                static if (uda == "updatableField") {
                                    ret ~= "if (__updatableFields[\"" ~ name ~ "\"])" ~
                                        "req[\"" ~ name ~ "\"] = serializeToBson((cast(" ~ T.stringof ~ ")this)." ~ name ~ ");";
                                }
                            }
                        }
                    }
                }
            }

            ret ~=
                q{logDebug("Updating entity in collection \"" ~ _collectionName ~ "\" with value " ~ req.toString ~ " for _id " ~ _id.toString ~ ".");} ~

                T.stringof ~ " res = deserializeBson!" ~ T.stringof ~ "(" ~ q{collection.findAndModify(Bson(_id), ["$set": req], UpdateFlags.Upsert)} ~ ");";

            ret ~=update();

            ret ~= q{
                delete res;

                __initUpdate();
            } ~ "}}";

        return ret;
    }

    /++
        Remove the current item from the datastore.
    +/
    private string remove() {
        string ret =
            "void remove() {" ~ q{
                logDebug("Removing entity in collection \"" ~ _collectionName ~ "\" with _id " ~ _id.toString ~ ".");
                Bson req = Bson.emptyObject;
                req["findAndModify"] = Bson(_collectionName);
                req["query"] = Bson(["_id": Bson(_id)]);
                req["remove"] = Bson(true);
                } ~

                T.stringof ~ " res = deserializeBson!" ~ T.stringof ~ "(MGClient.getDatabase(MGDataBase).runCommand(req).value);" ~
                update() ~

                q{
                delete res;
                _id = BsonObjectID.fromString("000000000000000000000000");
                } ~

            "}";

        return ret;
    }


    /++
        This function returns a string that represents a generated subclass of your class
        that should be called in a mixin. It allows you to do things like D(saveToDatastore)
        with update instead of insert.

        Mark all setters you want to register with the D(@updatableField) UDA. All setter must
        have a corresponding getter, for the deserialization.
    +/
    public string Store() {
        // Generate class:
        string ret = "public static class _" ~ T.stringof ~ " : " ~ T.stringof ~ " {";

        // Constructor:
        ret ~= q{
            public this() {
                __registerUpdatableFields();
                super();
                __initUpdate();
            }
        };

        foreach(name; __traits(allMembers, T)) {
            static if (__traits(getProtection, mixin("T." ~ name)) == "public") {
                foreach(symbol; __traits(getOverloads, T, name)) {
                    static if (isCallable!symbol && (functionAttributes!symbol & FunctionAttribute.property)) {
                        static if ((ParameterTypeTuple!symbol).length > 0) {
                            ret ~= function string() {
                                foreach (uda; __traits(getAttributes, symbol)) {
                                    static if (uda == "updatableField") {
                                        // Overriding updatable field setter:
                                        return "override @property " ~ (ReturnType!symbol).stringof ~ " " ~ name ~
                                            "(" ~ (ParameterTypeTuple!symbol[0]).stringof ~ " value) {__updatableFields[\"" ~ name ~ "\"] = true;" ~
                                            "super." ~ name ~ "(value);}";
                                    }
                                }
                                // Overriding non updatable field setter:
                                return "override @property " ~ (ReturnType!symbol).stringof ~ " " ~ name ~
                                    "(" ~ (ParameterTypeTuple!symbol[0]).stringof ~ " value) {" ~
                                    "super." ~ name ~ "(value);}";
                            }();
                        } else {
                            ret ~= function string() {
                                foreach (uda; __traits(getAttributes, symbol)) {
                                    static if (uda == "updatableField") {
                                        // Overriding updatable field getter:
                                        return "override @property " ~ (ReturnType!symbol).stringof ~ " " ~ name ~ "() {__updatableFields[\"" ~ name ~ "\"] = true;" ~
                                            "return super." ~ name ~ ";}";
                                    }
                                }
                                // Overriding non updatable field getter:
                                return "override @property " ~ (ReturnType!symbol).stringof ~ " " ~ name ~ "() {" ~
                                    "return super." ~ name ~ ";}";
                            }();
                        }
                    }
                }
            }
        }

        // End of the generated class
        ret ~= "}";

        // Generated class alias
        ret ~= "alias __classType = _" ~ T.stringof ~ ";";

        // Register the fields that should be updated in the datastore when modified in the program.
        ret ~= "protected void __registerUpdatableFields() {";
            foreach(name; __traits(allMembers, T)) {
                static if (__traits(getProtection, mixin("T." ~ name)) == "public") {
                    foreach(symbol; __traits(getOverloads, T, name)) {
                        static if (isCallable!symbol && (functionAttributes!symbol & FunctionAttribute.property)) {
                            foreach (UDA; __traits(getAttributes, symbol)) {
                                static if (UDA == "updatableField") {
                                    ret ~= "__updatableFields[\"" ~ name ~ "\"] = false;";
                                }
                            }
                        }
                    }
                }
            }
        ret ~= "}";

        /++ The subclass name, defined at compile time. +/
        ret ~=
            q{
                import std.traits;
                protected string _collectionName = __defaultCollectionName;
            } ~
            "protected static immutable string __defaultCollectionName = fullyQualifiedName!" ~ T.stringof ~ ";";

        ret ~= store();

        ret ~= remove();

        // Generate datastore wrappers
        ret ~= q{
            static __classType getById(string id) {
                __classType[] ret = getFromDatastore(Bson(["_id": Bson(BsonObjectID.fromString(id))]));
            if (ret.length <= 0)
                return null;
            return ret[0];
            }

            static __classType[] getFromDatastore(Bson req, string collectionName = null) {
                if (collectionName is null)
                    collectionName = __defaultCollectionName;
                logDebug("Getting datastore objects with request " ~ req.toString ~ " in collection \"" ~ collectionName ~ "\".");
                MongoCollection collection = MGCollection(collectionName);
                auto cursor = collection.find(req);
                __classType[] ret;
                foreach (doc ; cursor) {
                    ret ~= deserializeBson!__classType(doc);
                }
                return ret;
            }
        };

        return ret;
    }
}

/++ Base class for storable entities. +/
public abstract class Storable {
private:
    /++ The datastore ID. +/
    BsonObjectID _dsId;

protected:
    /++ The updated fields booleans array. +/
    bool[string] __updatableFields;

    /++
        Set all the updatable fields to no updated.
    +/
    void __initUpdate() {
        logDebug("Initializing update...");
        foreach (string name, bool value; __updatableFields)
            __updatableFields[name] = false;
    }

public:
    /++ Constructor. Just set the ID to null. +/
    this () {this._id = BsonObjectID.fromString("000000000000000000000000");}

    /++ The datastore ID. +/
    @property void _id(BsonObjectID id) { _dsId = id; }

    ///ditto
    @property BsonObjectID _id() { return _dsId; }
}
