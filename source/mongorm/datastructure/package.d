/++
    Authors: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: Subject to the terms of the MIT license, as written in the included LICENSE file.
 +/
module mongorm.datastructure;

public import mongorm.datastructure.base;
