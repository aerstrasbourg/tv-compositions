/++
    Authors: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: Subject to the terms of the MIT license, as written in the included LICENSE file.
 +/
module mongorm.storage;

import vibe.d;

import compositions.utils.params;

private static MongoClient _MG;
private static string _dataBase;

/++ Get the current Mongo client. +/
@property
ref MongoClient MGClient() {
    return _MG;
}

/++ Get the current database. +/
@property
ref string MGDataBase() {
    return _dataBase;
}

/++ Get the collection corresponding to the parameter. +/
auto MGCollection(string collection) {
    return _MG.getCollection(_dataBase~"."~collection);
}

/++ Module initializer. +/
shared static this() {
    _dataBase = Params.get("MG_DB");
    _MG = connectMongoDB("mongodb://"~Params.get("MG_USER")~":"~Params.get("MG_PWD")~
                        "@"~Params.get("MG_HOST")~":"~Params.get("MG_PORT")~
                        "/"~_dataBase~"?"~Params.get("MG_OPT"));
}
