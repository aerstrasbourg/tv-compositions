/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 18, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.api.modules;

import vibe.d;

import compositions.storage;

/++
    Interface that defines the modules routes.
 +/
interface APIModuleInterface {
    /++
        $(ROUTE $(PATCH), /composition/:id/module/:mod)

        Update a module.

        Request_body:
        ---
            {
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                },
                "subModules": [
                    ...
                ]
            }
        ---

        Response_body:
        ---
            {
                "_id": "555cac8d5d01b76811000000",
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                },
                "subModules": [
                    ...
                ]
            }
        ---
     +/
    @path(":id/module/:mod")
    Json updateModule(string _id, string _mod, Json config, string type, string name, SComposition.Module[] subModules);

    /++
        $(ROUTE $(POST), /composition/:id/module/:mod/slot/:slot)

        Create a new module in the module :mod at the slot :slot.

        Request_body:
        ---
            {
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                },
                "subModules": [
                    ...
                ]
            }
        ---

        Response_body:
        ---
            {
                "_id": "555cac8d5d01b76811000000",
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                },
                "subModules": [
                    ...
                ]
            }
        ---
     +/
    @path(":id/module/:mod/slot/:slot")
    Json putModule(string _id, string _mod, string _slot, string type, Json config, string name, SComposition.Module[] subModules);

    /++
        $(ROUTE $(DELETE), /composition/:id/module/:mod/)

        Delete the module :mod in the composition.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            {
            }
        ---
     +/
    @path(":id/module/:mod")
    Json deleteModule(string _id, string _mod);
}

/++ Implements the modules API. +/
class APIModule : APIModuleInterface {
public:
    this() {}

override:
    /++ $(ROUTE $(PATCH), /composition/:id/module/:mod) +/
    Json updateModule(string _id, string _mod, Json config, string type, string name, SComposition.Module[] subModules) {
        auto composition = SComposition.getById(_id);
        if (BsonObjectID.fromString(_mod) == composition._id) {
            if (config)
                composition.config = config;
            if (type)
                composition.type = type;
            if (name)
                composition.name = name;
            if (subModules)
                composition.subModules = subModules;
        } else {
            void updateSubModules(ref SComposition.Module mod) {
                foreach (ref SComposition.Module m ; mod.subModules) {
                    if (m.id == BsonObjectID.fromString(_mod)) {
                        if (config)
                            m.config = config;
                        if (type)
                            m.type = type;
                        if (name)
                            m.name = name;
                        if (subModules)
                            m.subModules = subModules;
                    }
                    updateSubModules(m);
                }
            }
            foreach (ref SComposition.Module m ; composition.subModules) {
                if (m.id == BsonObjectID.fromString(_mod)) {
                    if (config)
                        m.config = config;
                    if (type)
                        m.type = type;
                    if (name)
                        m.name = name;
                    if (subModules)
                        m.subModules = subModules;
                }
                updateSubModules(m);
            }
        }
        composition.subModules = composition.subModules;
        composition.store();
        return serializeToJson(composition);
    }

    /++ $(ROUTE $(PUT), /composition/:id/module/:mod/slot/:slot) +/
    Json putModule(string _id, string _mod, string _slot, string type, Json config, string name = "", SComposition.Module[] subModules = []) {
        auto composition = SComposition.getById(_id);
        if (BsonObjectID.fromString(_mod) == composition._id) {
            while (composition.subModules.length <= to!int(_slot))
                composition.subModules = composition.subModules ~ SComposition.Module(BsonObjectID.generate, "empty", "", Json.emptyObject, []);
            composition.subModules[to!int(_slot)] = SComposition.Module(BsonObjectID.generate, type, name, config, subModules);
        } else {
            void putSubModules(ref SComposition.Module mod) {
                foreach (ref SComposition.Module m ; mod.subModules) {
                    if (m.id == BsonObjectID.fromString(_mod)) {
                        while (m.subModules.length <= to!int(_slot))
                            m.subModules ~= SComposition.Module(BsonObjectID.generate, "empty", "", Json.emptyObject, []);
                        m.subModules[to!int(_slot)] = SComposition.Module(BsonObjectID.generate, type, name, config, subModules);
                    }
                    putSubModules(m);
                }
            }
            foreach (ref SComposition.Module m ; composition.subModules) {
                if (m.id == BsonObjectID.fromString(_mod)) {
                    while (m.subModules.length <= to!int(_slot))
                        m.subModules ~= SComposition.Module(BsonObjectID.generate, "empty", "", Json.emptyObject, []);
                    m.subModules[to!int(_slot)] = SComposition.Module(BsonObjectID.generate, type, name, config, subModules);
                }
                putSubModules(m);
            }
        }
        composition.subModules = composition.subModules;
        composition.store();
        return serializeToJson(composition);
    }

    /++ $(ROUTE $(DELETE), /composition/:id/module/:mod) +/
    Json deleteModule(string _id, string _mod) {
        auto composition = SComposition.getById(_id);
        if (BsonObjectID.fromString(_mod) == composition._id) {
            composition.subModules = [];
        } else {
            void deleteSubModules(ref SComposition.Module mod) {
                SComposition.Module[] nSubArr = [];
                foreach (ref SComposition.Module m ; mod.subModules) {
                    if (m.id != BsonObjectID.fromString(_mod)) {
                        deleteSubModules(m);
                        nSubArr ~= m;
                    }
                }
                mod.subModules = nSubArr;
            }
            SComposition.Module[] nArr = [];
            foreach (ref SComposition.Module m ; composition.subModules) {
                if (m.id != BsonObjectID.fromString(_mod)) {
                    deleteSubModules(m);
                    nArr ~= m;
                }
            }
            composition.subModules = nArr;
        }
        composition.subModules = composition.subModules;
        composition.store();
        return serializeToJson(composition);
    }
}
