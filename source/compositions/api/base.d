/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.api.base;

import vibe.d;

import compositions.utils.error;
import compositions.api.composition;

/++
    Base API interface that defines all the first level api.
 +/
interface APIBaseInterface {
    /++
        $(ROUTE $(GET), /)

        The base composition route
     +/
    @path("/")
    @property APICompositionInterface composition();
}

/++
    Base API class that implements base interface.

    This is the enter point of the API. This class manage the calling
    of all the others API classes, like APIUsers.

    It must be used in the URLRouter when launching the server.
 +/
class APIBase : APIBaseInterface {
private:
    APIComposition _compositions;

public:
    /++
        Init all the api subclasses.
     +/
    this() {
        _compositions = new APIComposition();
    }

override:
    /++ $(ROUTE $(GET), /) +/
    @property APICompositionInterface composition() { return _compositions; }
}
