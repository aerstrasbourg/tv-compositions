/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.api;

public import compositions.api.base;
