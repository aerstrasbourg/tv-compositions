/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.api.composition;

import vibe.d;

import compositions.storage;
import compositions.api.modules;

/++ Composition API interface that define compositions relative methods. +/
interface APICompositionInterface {
    /++
        $(ROUTE $(POST), /composition)

        Create a new composition.

        Request_body:
        ---
            {
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                }
            }
        ---

        Response_body:
        ---
            {
            "_id": "555cac8d5d01b76811000000",
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                },
                "subModules": [
                    ...
                ]
            }
        ---
     +/
    Json addComposition(string type, string name, Json config);

    /++
        $(ROUTE $(GET), /composition)

        Get all compositions.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            [
                {
                    "_id": "555cac8d5d01b76811000000",
                    "type": "test type",
                    "name": "test name",
                    "config": {
                        ...
                    },
                    "subModules": [
                        ...
                    ]

                },
                ...
            ]
        ---
     +/
    Json getComposition();

    /++
        $(ROUTE $(GET), /composition/:id)

        Get a composition.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            {
                "_id": "555cac8d5d01b76811000000",
                "type": "test type",
                "name": "test name",
                "config": {
                    ...
                },
                "subModules": [
                    ...
                ]
            }
        ---
     +/
    @path("/composition/:id")
    Json getCompositionById(string _id);

    /++
        $(ROUTE $(DELETE), /composition/:id)

        Delete a composition.

        Request_body:
        ---
            {
            }
        ---

        Response_body:
        ---
            {
            }
        ---
     +/
    @path("/composition/:id")
    Json deleteComposition(string _id);

    /++
        $(ROUTE $(GET), /composition)

        The base module route.
     +/
    @path("/composition")
    @property APIModuleInterface getModule();
}

/++ Implements the compositions API. +/
class APIComposition : APICompositionInterface {
private:
    APIModule _module;

public:
    this() {
        _module = new APIModule();
    }

override:
    /++ $(ROUTE $(POST), /composition) +/
    Json addComposition(string type, string name, Json config) {
        auto composition = new SComposition;
        composition.type = type;
        composition.name = name;
        composition.config = config;
        composition.store();
        return serializeToJson(composition);
    }

    /++ $(ROUTE $(GET), /composition) +/
    Json getComposition() {
        auto compositions = SComposition.getFromDatastore(Bson.emptyObject);
        return serializeToJson(compositions);
    }

    /++ $(ROUTE $(GET), /composition/:id) +/
    Json getCompositionById(string _id) {
        auto composition = SComposition.getById(_id);
        return serializeToJson(composition);
    }

    /++ $(ROUTE $(DELETE), /composition/:id) +/
    Json deleteComposition(string _id) {
        auto composition = SComposition.getById(_id);
        composition.remove();
        return Json.emptyObject;
    }

    /++ $(ROUTE $(GET), /composition) +/
    @property APIModuleInterface getModule() {
        return _module;
    }
}
