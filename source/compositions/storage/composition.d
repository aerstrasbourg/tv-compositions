/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.storage.composition;

import mongorm;

/++
    A class that represents a composition in the datastore.
 +/
class SComposition : Storable {
public:
    mixin(Store!SComposition);

    struct Module {
        BsonObjectID id;
        string type;
        string name;
        Json config;
        Module[] subModules;

        this(BsonObjectID id, string type, string name, Json config, Module[] subModules) {
            this.id = id;
            this.type = type;
            this.name = name;
            this.config = config;
            this.subModules = subModules;
        }
    }

    /++
        The composition type.
     +/
    @update @property void type(string arg) { _type = arg; }

    ///ditto
    @property string type() { return _type; }

    /++
        The composition name.
     +/
    @update @property void name(string arg) { _name = arg; }

    ///ditto
    @property string name() { return _name; }

    /++
        The composition arg.
     +/
    @update @property void config(Json arg) { _config = arg; }

    ///ditto
    @property Json config() { return _config; }

    /++
        The composition modules.
     +/
    @update @property void subModules(Module[] arg) { _modules = arg; }

    ///ditto
    @property Module[] subModules() { return _modules; }

private:
    Module[] _modules;
    string _type;
    string _name;
    Json _config;
}
