/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.utils.params;

import std.json;
import std.conv;
import std.file;
import std.stdio;
import std.process;

import vibe.d;

/++
    Contains all the server's configurations.

    It parses a configuration file defined staticaly, and then you can
    get all the configs defined in.

    This class cannot be instancied.
 +/
final class Params {
private:
    /++ The path to the configuration file. +/
    static const string CONFIG_FILE_PATH = "./server_config.json";

    static bool _initialized = false;

    /++ The configs are stored in this object. +/
    static JSONValue _configs;

    /++ Private empty constructor, never called. +/
    this () {}

    /++
        Parse the config file defined with CONFIG_FILE_PATH and fill
        the attribute _configs with the file's datas.
     +/
    static void _parseFile() {
        logDebug("Parsing configs from file " ~ CONFIG_FILE_PATH);
        auto content = to!string(read(CONFIG_FILE_PATH));
        _configs = parseJSON(content).object;
        _initialized = true;
    }

public:
    /++
        Get a config by key.

        Params:
            key = the param's key.

        Returns: The corresponding config.
     +/
    static auto get(string key) {
        if (_initialized == false)
            init();
        logDebug("Getting the config " ~ key);
        return environment.get(key, (_configs.object[key]).str);
    }

    /++
        Initialize the class, if not already done.
     +/
    static void init() {
        if (_initialized == false) {
            _parseFile();
        }
    }
}

/++
    Initialize the Params class when including the module for the first time.
 +/
shared static this () {
    Params.init();
}
