/++
    Author: Matthieu Kern, matthieukern@gmail.com
    Date: May 16, 2015
    Copyright: Matthieu Kern
	License: MIT
 +/
module compositions.utils.error;

import std.conv;
import std.stdio;

import vibe.data.json;
import vibe.d;

import compositions.utils.params;

/++
    Throwable error.
 +/
class err : Throwable {
public:
    /++
        Constructor

        Params:
            msg = the message that must be thrown.
            code = the error code.
            file = the file name where the error has occured.
            line = the line the error has occured.
            next = next throwable object.
     +/
    @safe pure nothrow this(string msg = "An error has occured.", int code = 0, string file = __FILE__,
                            size_t line = __LINE__, Throwable next = null) {
        super(msg, file, line, next);
        this.code = code;
    }

    @safe nothrow this(int code, Throwable next = null, string file = __FILE__, size_t line = __LINE__) {
        if (!(code in _codes))
            code = 0;
        super(_codes[code], file, line, next);
        this.code = code;
    }

    /++
        <dt>Error codes</dt>
        <dd>
            <table class="table table-condensed table-bordered">
                <tr><td>1</td><td>An error has occured.</td></tr>
                <tr><td>2</td><td>Bad parameter.</td></tr>
                <tr><td>3</td><td>Feature not implemented yet.</td></tr>
                <tr><td>10</td><td>MongoDB error.</td></tr>
                <tr><td>11</td><td>"No corresponding entry in the datastore.</td></tr>
            </table>
        </dd>
    +/
    int code;
}

enum {
    // 1     -> 9        Commons errors
    ERR_OCCURED = 1,
    BAD_PARAMETER,
    NOT_IMPL,
    // 10    -> 19       MongoDB errors
    MG_ERROR = 10,
    MG_NO_ENTRY
}

/++ The associative array of errors. +/
private static const string[int] _codes;

/++
    Module initialization

    Fills the errors array
 +/
shared static this() {
    _codes = [
        // Commons errors
        ERR_OCCURED:    "An error has occured.",
        BAD_PARAMETER:  "Bad parameter.",
        NOT_IMPL:       "Feature not implemented yet.",

        // MongoDB errors
        MG_ERROR:       "MongoDB error.",
        MG_NO_ENTRY:    "No corresponding entry in the datastore."
    ];
}

/++
    Print an error to the server console.

    Params:
        e = The error to print.
+/
void printError(Throwable e) {
    bool first = true;
    string toDisp;
    while (e !is null) {
        if (first)
            toDisp = "Error ";
        else
            toDisp = "From ";
        if (err casted = cast(err)e) {
            toDisp ~= "in " ~ casted.file ~ " line " ~ to!string(e.line) ~ ": error " ~ to!string(casted.code) ~ " \"" ~ casted.msg ~ "\"";
        } else {
            toDisp ~= "in " ~ e.file ~ " line " ~ to!string(e.line) ~ ": exception \"" ~ e.msg ~ "\"";
        }
        logError(toDisp);
        e = e.next;
        first = false;
    }
}

/++
    Function that catch the errors and return the corresponding JSON if needed.

    Params:
        e = the error that has been throwed.

    Returns: The error corresponding JSON.
 +/
Json catchError(Throwable e) {
    Json ret = Json.emptyObject;
    printError(e);
    if (err casted = cast(err)e) {
        ret["success"] = false;
        ret["code"] = casted.code;
    } else {
        ret["success"] = false;
        ret["code"] = 0;
    }
    return ret;
}
